FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir -p /code/media
WORKDIR /code
COPY requirements.txt /code/
COPY .env /.env
COPY init.sh /init.sh
RUN chmod 755 /init.sh
RUN pip3 install -r requirements.txt
COPY . /code
CMD ["/init.sh"]
