const links = [
    {
        link: "/favourite-things",
        text: "Favourite things"
    },
    {
        link: "/categories",
        text: "All Categories"
    },
    {
        link: "/logs",
        text: "Activity Logs"
    },
    {
        link: "/logout",
        text: "Log out"
    }
]

export default links
