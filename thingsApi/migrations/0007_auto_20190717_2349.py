# Generated by Django 2.2.3 on 2019-07-17 23:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thingsApi', '0006_auto_20190717_2325'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favouritething',
            name='ranking',
            field=models.PositiveSmallIntegerField(blank=True),
        ),
    ]
