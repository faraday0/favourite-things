"""Test APIs."""
from django.test import TestCase
from thingsApi.models import CustomUser


class TestAPIs(TestCase):
    """Test all APIs."""

    def setUp(self):
        """Set up requirements and data."""
        self.user = CustomUser.objects.create(
            email='test@test.com',
            username='test_user',
            name='user'
        )

    def test_create_category():
        """Test that posting to /things/categories creates category."""
        pass

    def test_create_favourite_thing():
        """Test that /things/ creates favourite things."""
        pass

    def test_update_favourite_thing():
        """Test that /things/<int:id> updates favourite thing."""
        pass

    def test_fetch_favourite_thing():
        """Test that /things/<int:id> fetches favourite thing."""
        pass

    def test_update_fetch_category():
        """/things/categories/<int:id> fetches and updates category."""
        pass
